package cn.only.demo.service;

/**
 * author       : Wayne Hu
 * date created : Created in 2024/9/3 10:51
 * description  : Service
 * class name   : TestService
 */
public interface TestService {

    String test();
}
