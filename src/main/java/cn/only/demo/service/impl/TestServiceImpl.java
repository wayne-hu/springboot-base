package cn.only.demo.service.impl;

import cn.only.demo.service.TestService;
import org.springframework.stereotype.Service;

/**
 * author       : Wayne Hu
 * date created : Created in 2024/9/3 10:52
 * description  : Service实现类
 * class name   : TestServiceImpl
 */
@Service
public class TestServiceImpl implements TestService {

    @Override
    public String test() {
        return "test";
    }


}
