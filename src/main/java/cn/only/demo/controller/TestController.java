package cn.only.demo.controller;

import cn.only.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * author       : Wayne Hu
 * date created : Created in 2024/9/3 10:50
 * description  : Test
 * class name   : TestController
 */
@RestController
public class TestController {
    @Autowired
    private TestService testService;

    @GetMapping("/test")
    public String test(){
        return testService.test();
    }
}
