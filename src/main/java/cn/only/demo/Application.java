package cn.only.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * author       : Wayne Hu
 * date created : Created in 2024/9/3 10:52
 * description  : 启动类
 * class name   : Application
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
